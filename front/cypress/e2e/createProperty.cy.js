describe('Create and delete a property', () => {
  it('passes', () => {
    cy.visit('/property')
    cy.get('.occupant-name-input').type('Michel')
    cy.get('.address-autocomplete-input').type('2')
    cy.wait(500)
    cy.get('.address-autocomplete-input').type('{downArrow}')
    cy.wait(500)
    cy.get('body').type('{enter}')
    cy.wait(500)
    cy.get('button[type=submit]').click()
    cy.wait(500)

    cy.get('.properties-list tr:last-child .mdi-delete').click()

    cy.wait(500)

  })
})