import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import { Point } from 'ol/geom';
import Feature from 'ol/Feature.js';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import OSM from 'ol/source/OSM.js';
import TileLayer from 'ol/layer/Tile.js';
import Style from 'ol/style/Style';
import Icon from 'ol/style/Icon';


/**
 * Creates a map with OpenLayer.
 * 
 * @param {Array} centerLonLat - array containing map center coordinates.
 * @returns 
 */
export function createMap(centerLonLat) {
    return new Map({
        target: 'map',
        layers: [
            new TileLayer({
                source: new OSM()
            })
        ],
        view: new View({
            center: centerLonLat,
            zoom: 12
        })
    });
}

/**
 * Add marker on existing map. 
 * 
 * Return created layer. 
 * 
 * @param {ol/Map} map 
 * @param {Array} lonLat 
 * @returns 
 */
export function addMarkerToMap(map, lonLat, hightight = false) {

    const layer = new VectorLayer({
        name: "marker",
        source: new VectorSource({
            features: [
                new Feature({
                    geometry: new Point(lonLat)
                })
            ]
        }),

        style: new Style({
            image: new Icon({
                anchor: [0.5, 1],
                src: hightight ?'marker_select.png' : 'marker.png'
            })
        })
    });

    map.addLayer(layer);

    return layer;
}

/**
 * Removes all markers from a map.
 * 
 * @param {ol/Map} map 
 */
export function removesAllMarkersToMap(map) {
    map.getLayers().forEach(layer => {
        if (layer && layer.get('name') === 'marker') {
            map.removeLayer(layer);
        }
    });
}