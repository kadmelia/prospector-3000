/**
 * Get all addresses from API.
 * 
 * @returns {Array} - List of all addresses
 */
export async function getAddresses(offset = 0, limit = 10) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + `/address?offset=${offset}&limit=${limit}`);
    const addresses =  await response.json();
    return addresses;

}

/**
 * Get a specific address from API.
 * 
 * @param {Number} id
 * @returns {Object} - Address
 */
export async function getAddress(id) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + '/address/' + id);
    const address =  await response.json();
    return address;
}
