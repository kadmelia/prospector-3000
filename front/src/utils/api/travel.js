
/**
 * 
 * @param {array} addresses
 * @returns {array} - Addresses ordered by optimized travel  
 */
export async function getOrderedAddressesByOptimizedTravel(addressesIds) {
    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + `/travel?addresses=${addressesIds.join(',')}`);
    const addresses =  await response.json();
    return addresses;
}