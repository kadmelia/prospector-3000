/**
 * Get all properties from API.
 * 
 * @returns {Array} - List of all properties
 */
export async function getProperties(offset = 0, limit = 10) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + `/property?offset=${offset}&limit=${limit}`);
    const properties =  await response.json();
    return properties;

}

/**
 * Create property from API.
 * 
 * @returns {Object} - created property
 */
export async function createProperty(property) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + `/property`, {
        method: 'POST', 
        body: JSON.stringify(property),
        headers: {
            "Content-Type": "application/json"
        }
    });
    const createdProperty =  await response.json();
    return createdProperty;

}

/**
 * Updates property from API.
 * 
 * @returns {Object} - updated property
 */
export async function updateProperty(property) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + `/property/${property.id}`, {
        method: 'PUT', 
        body: JSON.stringify(property),
        headers: {
            "Content-Type": "application/json"
        }
    });
    const updatedProperty =  await response.json();
    return updatedProperty;

}

/**
 * Get a specific property from API.
 * 
 * @param {Number} id
 * @returns {Object} - Property
 */
export async function getProperty(id) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + '/property/' + id);
    const property =  await response.json();
    return property;
}

/**
 * Delete a specific property from API.
 * @param {Number} id 
 */
export async function deleteProperty(id) {

    const response = await fetch(import.meta.env.VITE_API_ENDPOINT + '/property/' + id, { 
        method: 'DELETE',
    });
    if (response.status !== 204) {
        throw new Error("Error while deleting property");
    }

}