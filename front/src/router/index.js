import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/layouts/default/Default.vue'),
      children: [
        {
          path: 'travel',
          name: 'Itineraire',

          component: () => import('@/views/Travel.vue'),
        },
        {
          path: 'properties',
          name: 'Propriétés',

          component: () => import('@/views/Properties.vue'),
        },
        {
          path: 'property',
          name: 'Nouveau bien',

          component: () => import('@/views/Property.vue'),
        },
        {
          path: 'property/:id',
          name: 'Modifier le bien',

          component: () => import('@/views/PropertyEdit.vue'),
        },
      ],
    },
  
  ]
})

export default router
