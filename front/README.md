# Prospector 3000 - Frontend

Application frontend

## Configuration

Configure .env by copying .env.local and by adapting it's values to your environment.

```bash
cp .env.local .env
```

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```