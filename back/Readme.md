# Propspector 3000

Prospector 3000's backend

## Database creation
In order to work, Prospector 3000 need a working Postgres database with pgRouting and postgis installed.

On a debian-like, follow these steps.

Add postgres Apt repo [https://wiki.postgresql.org/wiki/Apt](https://wiki.postgresql.org/wiki/Apt)


Install packages

```bash 
sudo apt install postgresql-15 postgresql-contrib-15 postgresql-15-postgis-3 postgresql-15-pgrouting
```

Connect as root : 

```bash 
sudo -u postgres psql
```

Create user and DB, install needed extensions. In this exemple we will use "prospector", but you can use anything you want.

```SQL
CREATE ROLE prospector WITH LOGIN PASSWORD 'prospector';
CREATE DATABASE prospector OWNER prospector;
\c prospector
CREATE EXTENSION postgis;
CREATE EXTENSION pgRouting;
```

Then, import database schema, it's split in 2 files, CRM and graph.

```bash
psql -U prospector -d prospector -W < db/routing_schema_data.sql
psql -U prospector -d prospector -W < db/crm_schema.sql
```

## DB seeding

To add exemple data you need to execute `seed.js` script.

To allow it to connect to DB, you need to set your environment variables : 

```bash
cp .env.local .env
```

And edit with your data if needed.

Execute script : 

```bash
node seed.js
```


## Starting application

Configure .env by copying .env.local and by adapting it's values to your environment.

```bash
cp .env.local .env
```

To start the application in developper's mode (watcher)

```bash
npm install
npm run watch
```

To simply start the application in developper's mode (watcher)

```bash
npm start
```
