import {db} from "./db.js";

/**
 * Optimizes a travel between addresses.
 * 
 * @param {Array} addresses - each element must be an instance of Address.  
 * @returns Array
 */
export async function getOptimizedRouteForAddresses(addresses) {

    const osmPoints = [];
    const addressesAsVertexId = {};
    for (const address of addresses) {
        // Get nearest vertex of our OSM graph for each address 
        const vertexId = await address.getNearestOsmPoint();
        osmPoints.push(vertexId);
        addressesAsVertexId[vertexId] = address; 
    }
    // Build the cost matrix query for each vertex
    const costMatrixQuery = `SELECT * FROM pgr_dijkstraCostMatrix(
    'SELECT gid as id, source, target, cost, reverse_cost FROM ways',
    (SELECT array_agg(id)
        FROM ways_vertices_pgr
        WHERE id IN (${osmPoints.join(',')})),
    false);`;
    
    // Use pgr_TSP to get optimized travel
    const query = `SELECT * FROM pgr_TSP($$${costMatrixQuery}$$)`;
    
    const result = await db.raw(query);
    
    const orderedAddresses = [];
    for (const vertex of result.rows) {
        // Get address for each ordered vertex
        orderedAddresses.push(addressesAsVertexId[vertex.node]);
    }
    
    // Remove last item which is return to starting point.
    orderedAddresses.pop();
    
    return orderedAddresses;
}