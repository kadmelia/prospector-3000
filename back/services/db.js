import knex from 'knex';
import knexPostgis from 'knex-postgis';

export const db = knex({
    client: 'pg',
    connection: process.env.DATABASE_URL
});


export const postgis = knexPostgis(db);