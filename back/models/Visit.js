import { db } from "../services/db.js";

export class Visit {
    static tableName = 'visit';
    static schema = {
        type: 'object',
        properties: {
            date: { type: 'string' },
            comment: { type: 'string' },
            extra: { type: 'object' },
            property_id: { type: 'string' },
        },
        required: ['date', 'property_id']
    }

    /**
     * Class constructor.
     * 
     * @param {Object} params 
     */
    constructor(params) {
        this.id = params?.id;
        this.updateData(params);
    }

    /**
     * Update object data.
     * 
     * @param {Object} params 
     */
    updateData(params) {
        this.date = params.date;
        this.comment = params.comment;
        this.extra = params.extra;
        this.property_id = params.property_id;
    }

    /**
     * Perform an upsert on DB.
     */
    async save() {

        const data = {
            date: this.date,
            comment: this.comment,
            extra: this.extra,
            property_id: this.property_id
        };
        let saved;

        if (this.id === undefined) {
            saved = await db(Visit.tableName).insert(data).returning("*");
        } else {
            saved = await db(Visit.tableName).where({ id: this.id }).update(data).returning("*");
        }

        this.id = saved[0].id;

    }

    /**
     * Delete address in DB.
     */
    async delete() {
        await db(Visit.tableName).where('id', this.id).del();
    }

    /**
     * Get a specific address.
     * 
     * @param {Number} id 
     * @returns Visit 
     */
    static async get(id) {

        const results = await db(Visit.tableName).select("id", "date", "comment", "extra", "property_id").where({
            id
        });

        if (results.length === 0) {
            throw new Error(`Visit ${id} not found`);
        }

        return new Visit(results[0]);

    }

    /**
      * Find visits.
      * 
      * @param {Object} criterias 
      * @returns Visit 
      */
    static async find(criterias = {}, offset = 0, limit = 10) {

        const results = await db(Visit.tableName)
            .select()
            .where(criterias)
            .limit(limit)
            .offset(offset);

        const data = [];

        for (const row of results) {
            data.push(new Visit(row));
        }

        return data;

    }

    async loadAddress() {
        this.address = await Address.get(this.address_id);
    }

}