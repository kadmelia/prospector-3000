import { db, postgis } from "../services/db.js";
import { Address } from "./Address.js";
import { Visit } from "./Visit.js";

export class Property {
    static tableName = 'property';
    static schema = {
        type: 'object',
        properties: {
            occupant_name: { type: 'string' },
            occupant_phone: { type: 'string' },
            occupant_email: { type: 'string' },
            extra: { type: 'object' },
            address_id: { type: 'string' },
        },
        required: ['occupant_name', 'address_id']
    }

    /**
     * Class constructor.
     * 
     * @param {Object} params 
     */
    constructor(params) {
        this.id = params?.id;
        this.updateData(params);
    }

    /**
     * Update object data.
     * 
     * @param {Object} params 
     */
    updateData(params) {
        this.occupant_name = params.occupant_name
        this.occupant_phone = params.occupant_phone
        this.occupant_email = params.occupant_email
        this.extra = params.extra
        this.address_id = params.address_id
    }

    /**
     * Perform an upsert on DB.
     */
    async save() {

        const data = {
            occupant_name: this.occupant_name,
            occupant_phone: this.occupant_phone,
            occupant_email: this.occupant_email,
            extra: this.extra,
            address_id: this.address_id
        };
        let saved;

        if (this.id === undefined) {
            saved = await db(Property.tableName).insert(data).returning("*");
        } else {
            saved = await db(Property.tableName).where({ id: this.id }).update(data).returning("*");
        }

        this.id = saved[0].id;

    }

    /**
     * Delete address in DB.
     */
    async delete() {
        await db(Property.tableName).where('id', this.id).del();
    }

    /**
     * Get a specific address.
     * 
     * @param {Number} id 
     * @returns Property 
     */
    static async get(id) {

        const results = await db(Property.tableName).select("id", "occupant_name", "occupant_phone", "occupant_email", "extra", "address_id").where({
            id
        });

        if (results.length === 0) {
            throw new Error(`Property ${id} not found`);
        }
        const data = {
            ...results[0],
            // position : JSON.parse(results[0].position).coordinates
        }

        return new Property(data);

    }

    /**
      * Find properties.
      * 
      * @param {Object} criterias 
      * @returns Property 
      */
    static async find(criterias = {}, offset = 0, limit = 10) {

        const results = await db(Property.tableName)
            .select()
            .where(criterias)
            .limit(limit)
            .offset(offset);

        const data = [];

        for (const row of results) {
            data.push(new Property(row));
        }

        return data;

    }

    /**
     * Load Property address in "address" property.
     */
    async loadAddress() {
        this.address = await Address.get(this.address_id);
    }

    async loadVisits() {
        this.visits = await Visit.find({
            "property_id": this.id
        })
    }

}