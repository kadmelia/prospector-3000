import { db, postgis } from "../services/db.js";

export class Address {
    static tableName = 'address';

    /**
     * JSON schema used for validation
     */
    static schema = {
        type: 'object',
        properties: {
            address: { type: 'string' },
            zipcode: { type: 'string' },
            city: { type: 'string' },
            position: { type: 'array' },
        },
        required: ['occupant_name', 'address_id']
    }

    /**
     * Class constructor.
     * 
     * @param {Object} params 
     */
    constructor(params) {
        this.id = params?.id;
        this.updateData(params);
    }

        /**
     * Update object data.
     * 
     * @param {Object} params 
     */
    updateData(params) {
        this.address = params.address;
        this.zipcode = params.zipcode;
        this.city = params.city;
        this.position = params.position;
    }

    /**
     * Perform an upsert on DB.
     */
    async save() {

        const data = {
            address: this.address,
            zipcode: this.zipcode,
            city: this.city,
            position: postgis.setSRID(postgis.makePoint(this.position[0], this.position[1]), 4326)
        };
        let saved;

        if (this.id === undefined) {
            saved = await db(Address.tableName).insert(data).returning("*");
        } else {
            saved = await db(Address.tableName).update(data).returning("*");
        }
        
        this.id = saved[0].id;

    }

    /**
     * Delete address in DB.
     */
    async delete() {
        await db(Address.tableName).where('id', this.id).del();
    }

    /**
     * Get a specific address.
     * 
     * @param {Number} id 
     * @returns Address 
     */
    static async get(id) {

        const results = await db(Address.tableName).select('id', 'address', 'zipcode', 'city', postgis.asGeoJSON('position')).where({
            id
        });

        if (results.length === 0) {
            throw new Error(`Address ${id} not found`);
        }
        const data = {
            ...results[0],
            position : JSON.parse(results[0].position).coordinates
        }

        return new Address(data);

    }

    /**
      * Find addresses.
      * 
      * @param {Object} criterias 
      * @returns Address 
      */
    static async find(criterias = {}, offset = 0, limit = 10) {

        const results = await db(Address.tableName)
        .select('id', 'address', 'zipcode', 'city', postgis.asGeoJSON('position'))
        .where(criterias)
        .limit(limit)
        .offset(offset);

        const addresses = [];

        for (const row of results) {
            const data = {
                ...row,
                position : JSON.parse(results[0].position).coordinates
            }
            addresses.push(new Address(data));
        }

        return addresses;

    }

    /**
     * Retrieve nearest OSM vertex on graph.
     * 
     * @returns Number - vertex id
     */
    async getNearestOsmPoint() {
        const sql = `SELECT DISTINCT ON (a.id) a.id, v.id, ST_Distance(v.the_geom, a.position) as dist 
        FROM ways_vertices_pgr as v, address as a
        WHERE a.id = ${this.id}
        ORDER BY a.id, ST_Distance(v.the_geom, a.position) 
        LIMIT 1;`
        const result = await db.raw(sql);
    
        return result.rows[0].id;
    }

}