import { Address } from "../models/Address.js";

/**
 * Routes for traveling calculation.
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
export default async function routesAddress(fastify, options) {
    fastify.get('/address', async (request, reply) => {

        const params = {
            offset: 0,
            limit: 10,
            nested: true,
            ...request.query
        };

        const addresses = await Address.find({}, params.offset, params.limit);

        return addresses;
    });

    fastify.get('/address/:addressId', async (request, reply) => {

        const { addressId } = request.params;
        const address = await Address.get(addressId);

        return address;
    });

    fastify.post('/address', {
        async handler(request, reply) {

            const address = new Address(request.body);
            await address.save();

            return address;
        },
        schema: {
            body: Address.schema
        }
    });

    fastify.put('/address/:addressId', {
        async handler(request, reply) {

            const { addressId } = request.params;
            const data = request.body;
    
            const address = await Address.get(addressId);
            address.updateData(data);
            await address.save();
    
            return address;
        },
        schema: {
            body: Address.schema
        }
    });

    fastify.delete('/address/:addressId', async (request, reply) => {
        const { addressId } = request.params;

        const address = await Address.get(addressId);
        address.delete();

        reply.statusCode = 204;
    });
}