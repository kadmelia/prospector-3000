import { Property } from "../models/Property.js";

/**
 * Routes for traveling calculation.
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
export default async function routesTravel(fastify, options) {
    fastify.get('/property', async (request, reply) => {

        const params = {
            offset: 0,
            limit: 10,
            nested: true,
            ...request.query
        };

        const properties = await Property.find({}, params.offset, params.limit);

        if (params.nested) {
            for (const property of properties) {
                await property.loadAddress();
                await property.loadVisits();
            }
        }

        return properties;
    });

    fastify.get('/property/:propertyId', async (request, reply) => {

        const params = {
            nested: true,
            ...request.query
        };

        const { propertyId } = request.params;
        const property = await Property.get(propertyId);

        if (params.nested) {
            await property.loadAddress();
            await property.loadVisits();
        }

        return property;
    });

    fastify.post('/property', {
        async handler(request, reply) {
            // Remove id property on Property creation
            delete request.body.id;
            const property = new Property(request.body);
            await property.save();

            return property;
        },
        schema: {
            body: Property.schema
        }
    });

    fastify.put('/property/:propertyId', {
        async handler(request, reply) {

            const { propertyId } = request.params;
            const data = request.body;
    
            const property = await Property.get(propertyId);
            property.updateData(data);
            await property.save();
    
            return property;
        },
        schema: {
            body: Property.schema
        }
    });

    fastify.delete('/property/:propertyId', async (request, reply) => {
        const { propertyId } = request.params;

        const property = await Property.get(propertyId);
        property.delete();

        reply.statusCode = 204;
    });
}