import { Address } from "../models/Address.js";
import { getOptimizedRouteForAddresses } from "../services/travel.js";

/**
 * Routes for traveling calculation.
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
export default async function routes (fastify, options) {
    fastify.get('/travel', async (request, reply) => {

        if (typeof request.query.addresses === "undefined") {
            reply.status(400).send({
                statusCode: 400,
                error: "'addresses' parameter is mandatory"
            })
            return 
        }
        // gather all addresses to visit
        const ids = request.query.addresses.split(',');

        const addresses = [];
        for (const id of ids) {
            // Retrive addresses data
            addresses.push(await Address.get(id));
        }

        // get ordered addresses
        const orderedAddresses = getOptimizedRouteForAddresses(addresses);

        return orderedAddresses;

    });
}