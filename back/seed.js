import 'dotenv/config';
import { faker } from '@faker-js/faker/locale/fr';
import { Address } from './models/Address.js';
import { Property } from './models/Property.js';
import { Visit } from './models/Visit.js';

async function main() {
    const addresses = [{
            address: "3 rue de la résistance",
            zipcode: "42000",
            city: "Saint-Étienne",
            position: [4.385644132195559, 45.440150852872975]
        },
        {
            address: "28 rue de la Montat",
            zipcode: "42000",
            city: "Saint-Étienne",
            position: [4.396495930367581, 45.438504627211366]
        },
        {
            address: "20 Bd Augustin Thierry",
            zipcode: "42000",
            city: "Saint-Étienne",
            position:[4.382811748596393, 45.44676573693376]
        },
        {
            address: "78 Rue du Onze Novembre",
            zipcode: "42100",
            city: "Saint-Étienne",
            position:[4.391431053707186, 45.42405489222641]
        },
        {
            address: "8 Rue Notre Dame",
            zipcode: "42000",
            city: "Saint-Étienne",
            position:[ 4.390693182128632, 45.43571697671716]
        },
        {
            address: "12 Pl. Jean Jaurès",
            zipcode: "42000",
            city: "Saint-Étienne",
            position:[4.387221698883214, 45.44175891645028]
        },
        {
            address: '28 Rue Saint-Just',
            zipcode: "42000",
            city: "Saint-Étienne",
            position:[4.374323158051757, 45.4419763662394]
        },
        {
            address: '15 Rue Buffon',
            zipcode: "42100",
            city: "Saint-Étienne",
            position:[4.390965639262642, 45.41616127540804]
        },
        {
            address: '14 Rue Paul et Pierre Guichard',
            zipcode: "42000",
            city: "Saint-Étienne",
            position:[4.388560825378543, 45.46005314417942]
        },
        {
            address: '62 Rue Saint-Simon',
            zipcode: "42000",
            city: "Saint-Étienne",
            position:[4.368291500524596, 45.45428960595348]
        },
        {
            address: '91 Rue de la Croix de Mission',
            zipcode: "42100",
            city: "Saint-Étienne",
            position:[4.3699052029040715, 45.42660594280832]
        },
        {
            address: '90 Bd Alexandre de Fraissinette',
            zipcode: "42100",
            city: "Saint-Étienne",
            position:[4.420008415434203, 45.4190286897988]
        },
    ];

    for (const addressRaw of addresses) {
        const address = new Address(addressRaw);
        await address.save();
    }
    const propertiesCount = 30;

    for (let index = 0; index < propertiesCount; index++) {
        const property = new Property({
            occupant_name : faker.name.fullName(),
            occupant_phone : faker.phone.number(),
            occupant_email : faker.internet.email(),
            extra : {
                "stoppub": !!Math.floor(Math.random() * 2)
            },
            address_id : Math.ceil(Math.random() * addresses.length)
        });
        await property.save();
    }
    for (let index = 0; index < 20; index++) {
        const visit = new Visit( {
            date : faker.date.past(),
            comment : faker.lorem.words(10),
            extra : {
                "discuss": !!Math.floor(Math.random() * 2),
                "flyer": !!Math.floor(Math.random() * 2),
            },
            property_id : Math.ceil(Math.random() * propertiesCount)
        });

        await visit.save();
    }


    process.exit(1);    
}

main();