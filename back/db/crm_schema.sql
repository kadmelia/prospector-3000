BEGIN;

DROP TABLE IF EXISTS "address" CASCADE;
DROP SEQUENCE IF EXISTS "address_id_seq";
DROP TABLE IF EXISTS "visit" CASCADE;
DROP SEQUENCE IF EXISTS "visit_id_seq";
DROP TABLE IF EXISTS "property" CASCADE;
DROP SEQUENCE IF EXISTS "property_id_seq";

-- CRM schema

-- CreateTable
CREATE TABLE "property" (
    "id" INT GENERATED ALWAYS AS IDENTITY,
    "occupant_name" TEXT,
    "occupant_phone" TEXT,
    "occupant_email" TEXT,
    "extra" JSONB,
    "address_id" INTEGER NOT NULL,
    CONSTRAINT "property_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "address" (
    "id" INT GENERATED ALWAYS AS IDENTITY,
    "address" TEXT NOT NULL,
    "zipcode" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "position" public.geometry(Point,4326),
    CONSTRAINT "address_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "visit" (
    "id" INT GENERATED ALWAYS AS IDENTITY,
    "date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "extra" JSONB,
    "comment" TEXT,
    "property_id" INTEGER NOT NULL,
    CONSTRAINT "visit_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "property" ADD CONSTRAINT "property_address_id_fkey" FOREIGN KEY ("address_id") REFERENCES "address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "visit" ADD CONSTRAINT "visit_property_id_fkey" FOREIGN KEY ("property_id") REFERENCES "property"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

COMMIT;