import 'dotenv/config';
import { Address } from './models/Address.js';

import { getOptimizedRouteForAddresses } from './services/travel.js';



async function main() {
    // const list = await Address.get(1);
    // console.log(list);
    const test = await getOptimizedRouteForAddresses([
        await Address.get(1), 
        await Address.get(3),
        await Address.get(2)
    ]);
    console.log(test);
    process.exit(1);    
}

main();