// Import configuration from .env file
import 'dotenv/config';

import Fastify from 'fastify';
import cors from '@fastify/cors'
import travel from './routes/travel.js';
import properties from './routes/properties.js';
import addresses from './routes/address.js';

const fastify = Fastify({
  logger: true
});

await fastify.register(cors, { 
  // Disable CORS (yeah, i know, but hey, it's a prototype :grim:)
  origin: '*'
});

fastify.register(travel);
fastify.register(properties);
fastify.register(addresses);


fastify.listen({ port: process.env.PORT }, function (err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
})