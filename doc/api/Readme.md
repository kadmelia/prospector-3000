# API

The backend is available through an API. You can read detail on data [here](../data/Readme.md)

Note : to avoid confusion, all endoints names are singular.


In case of error : 

```json
{
    "statusCode": 500,
    "error": "Error name",
    "message": "Error message"

}
```


Here are the available API's endpoints : 

## /property

|HTTP Method|Description|
|---|---|
|GET|Get list of properties|
|POST|Create a new property|

### GET /property

Get a list of properties. The list is paginated, only 20 first are sent.

#### Arguments

Here the arguments list

`offset` : page number (start at 0)
`count` : Element per page
`nested` : get address data eager loaded in response (on by default)

#### Response

A response contains an array of properties with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|occupant_name|Text|Yes|
|occupant_phone|Text|Yes|
|occupant_email|Text|Yes|
|occupant_quality|Enum : tenant or landlord|Yes|
|extra|JSON|Yes|
|address_id|Number|Yes|

Example :  

```json
[
    {
        "id": 0,
        "occupant_name": "John Smith",
        "occupant_phone": "+33 6 05 04 08 09",
        "occupant_email": "john.smith@gmail.com",
        "extra": {
            "stop-pub": true,
            "empty": false
        },
        "address_id": 1,
        "address": {
            "id": 1,
            "address": "1 main street",
            "zipcode": "42000",
            "city": "Saint-Etienne",
            "location": [4.401603248127221, 45.4423448810578],
        }
    },
    ...
]
```

### POST /property

Create a property

#### Payload

Here the payload for creating a property

|Nom|Type|Can be null|
|---|---|---|
|occupant_name|Text|Yes|
|occupant_phone|Text|Yes|
|occupant_email|Text|Yes|
|extra|JSON|Yes|
|address_id|Number|Yes|

#### Response

A response contains the created property with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|occupant_name|Text|Yes|
|occupant_phone|Text|Yes|
|occupant_email|Text|Yes|
|extra|JSON|Yes|
|address_id|Number|Yes|

Example :  

```json
{
    "id": 0,
    "occupant_name": "John Smith",
    "occupant_phone": "+33 6 05 04 08 09",
    "occupant_email": "john.smith@gmail.com",
    "extra": {
        "stop-pub": true,
        "empty": false
    },
    "address_id": 1,
    "address": {
        "id": 1,
        "address": "1 main street",
        "zipcode": "42000",
        "city": "Saint-Etienne",
        "location": [4.401603248127221, 45.4423448810578],
    }
}
```

## /property/{id}

|HTTP Method|Description||
|---|---|---|
|GET|Get a specific property||
|PUT|Update a property||
|DELETE|Delete a property||

### GET /property/{id}

Arguments :

`nested` : get address data eager loaded in response (on by default)


#### Response

A response contains a property properties with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|occupant_name|Text|Yes|
|occupant_phone|Text|Yes|
|occupant_email|Text|Yes|
|occupant_quality|Enum : tenant or landlord|Yes|
|extra|JSON|Yes|
|address_id|Number|Yes|

Example :  

```json
{
    "id": 0,
    "occupant_name": "John Smith",
    "occupant_phone": "+33 6 05 04 08 09",
    "occupant_email": "john.smith@gmail.com",
    "occupant_quality": "tenant",
    "extra": {
        "stop-pub": true,
        "empty": false
    },
    "address_id": 1,
    "address": {
        "id": 1,
        "address": "1 main street",
        "zipcode": "42000",
        "city": "Saint-Etienne",
        "location": [4.401603248127221, 45.4423448810578],
    }
}
```

### PUT /property/{id}

Update a property

#### Payload

Here the payload for update a property

|Nom|Type|Can be null|
|---|---|---|
|occupant_name|Text|Yes|
|occupant_phone|Text|Yes|
|occupant_email|Text|Yes|
|extra|JSON|Yes|
|address_id|Number|Yes|

#### Response

A response contains the updated property with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|occupant_name|Text|Yes|
|occupant_phone|Text|Yes|
|occupant_email|Text|Yes|
|extra|JSON|Yes|
|address_id|Number|Yes|

Example :  

```json
{
    "id": 0,
    "occupant_name": "John Smith",
    "occupant_phone": "+33 6 05 04 08 09",
    "occupant_email": "john.smith@gmail.com",
    "extra": {
        "stop-pub": true,
        "empty": false
    },
    "address_id": 1,
    "address": {
        "id": 1,
        "address": "1 main street",
        "zipcode": "42000",
        "city": "Saint-Etienne",
        "location": [4.401603248127221, 45.4423448810578],
    }
}
```

### DELETE /property/{id}

Delete a property

#### Response

An HTTP response with a 204 code acknowledges full completion of the request.

### /travel

|HTTP Method|Description|
|---|---|
|GET|Get optimized route for visiting a property's list|


### GET /travel

Get ordered list of sent points optimized for visiting all points by quickest route. 

#### Arguments

Here the arguments list

`addresses` : ids of addresses to visit, comma separated

Format : 

http://api.example.com/travel?addresses=3,2,1

```
#### Response

Ordered list by travel optimization

Example :  

```json
[
  {
    "id": 1,
    "address": "3 rue de la résistance",
    "zipcode": "42000",
    "city": "Saint-Etienne",
    "position": [
      4.385644132,
      45.440150853
    ]
  },
  {
    "id": 2,
    "address": "28 rue de la Montat",
    "zipcode": "42000",
    "city": "Saint-Etienne",
    "position": [
      4.39649593,
      45.438504627
    ]
  },
  {
    "id": 3,
    "address": "20 Bd Augustin Thierry",
    "zipcode": "42000",
    "city": "Saint-Etienne",
    "position": [
      4.382811749,
      45.446765737
    ]
  }
]
```



/!\ Rest of API is not yet implemented /!\





## /property/{id}/visit

|HTTP Method|Description||
|---|---|---|
|GET|Get list of property's visits||
|POST|Create a new visit||

### GET /property/{id}/visit

Get a list of visits for a property. The list is paginated, only 20 first are sent.

#### Arguments

Here the arguments list

`offset` : page number (start at 0)
`count` : Element per page

#### Response

A response contains an array of properties with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|date|DateTime|No|
|extra|JSON|Yes|
|comment|Text|Yes|

Example :  

```json
{
    "count": 20,
    "offset": 0,
    "results": [
        {
            "id": 0,
            "date": "1977-04-22T01:00:00-05:00",
            "extra": {
                "discuss": {
                    "description": "Echange avec l'occupant",
                    "value": false
                },
                "flyer": {
                    "description": "Dépot d'un flyer dans la boite aux lettre",
                    "value": true
                },
            },
            "comment": "J'ai sonné 3 fois, il y avait clairement quelqu'un mais pas de réponse" 
        },
        ...
    ]
    
}
```

### POST /property/{id}/visit

Create a visit on a property

#### Payload

Here the payload for creating a visit

|Nom|Type|Can be null|
|---|---|---|
|date|DateTime|No|
|extra|JSON|Yes|
|comment|Text|Yes|

#### Response

A response contains the created visit with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|date|DateTime|No|
|extra|JSON|Yes|
|comment|Text|Yes|

Example :  

```json
{
    "results": {
        "id": 0,
        "date": "1977-04-22T01:00:00-05:00",
        "extra": {
            "discuss": {
                "description": "Echange avec l'occupant",
                "value": false
            },
            "flyer": {
                "description": "Dépot d'un flyer dans la boite aux lettre",
                "value": true
            },
        },
        "comment": "J'ai sonné 3 fois, il y avait clairement quelqu'un mais pas de réponse" 
    }
}
```

### /property/{id}/visit/{id}

|HTTP Method|Description||
|---|---|---|
|GET|Get a specific visit||
|PUT|Update a visit||
|DELETE|Delete a visit||

### GET /property/{id}/visit/{id}

#### Response

A response contains a visit with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|date|DateTime|No|
|extra|JSON|Yes|
|comment|Text|Yes|

Example :  

```json
{
    "results": {
        "id": 0,
        "date": "1977-04-22T01:00:00-05:00",
        "extra": {
            "discuss": {
                "description": "Echange avec l'occupant",
                "value": false
            },
            "flyer": {
                "description": "Dépot d'un flyer dans la boite aux lettre",
                "value": true
            },
        },
        "comment": "J'ai sonné 3 fois, il y avait clairement quelqu'un mais pas de réponse" 
    }
}
```

### PUT /property/{id}/visit/{id}

Update a visit

#### Payload

Here the payload tofor update a visit

|Nom|Type|Can be null|
|---|---|---|
|date|DateTime|No|
|extra|JSON|Yes|
|comment|Text|Yes|

#### Response

A response contains the updated visit with theses fields :   

|Nom|Type|Can be null|
|---|---|---|
|id|Number|No|
|date|DateTime|No|
|extra|JSON|Yes|
|comment|Text|Yes|

Example :  

```json
{
    "results": {
        "id": 0,
        "date": "1977-04-22T01:00:00-05:00",
        "extra": {
            "discuss": {
                "description": "Echange avec l'occupant",
                "value": false
            },
            "flyer": {
                "description": "Dépot d'un flyer dans la boite aux lettre",
                "value": true
            },
        },
        "comment": "J'ai sonné 3 fois, il y avait clairement quelqu'un mais pas de réponse" 
    }
}
```

### DELETE /property/{id}/visit/{id}

Delete a visit

#### Response

An HTTP response with a 204 code acknowledges full completion of the request.

