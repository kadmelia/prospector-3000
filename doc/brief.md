Compagnon de prospection physique.
Notre client, agent immobilier, à Saint-Étienne se prépare à aller prospecter sur le terrain à pied.
Comme il ne souhaite pas distribuer des flyers dans toute la ville, il a préparé une liste d’une dizaine de maisons pour lesquelles il souhaite entrer en contact avec le propriétaire ou déposer un flyer si celui-ci est absent.

User stories : 

Notre agent doit obtenir un itinéraire optimisé à partir des maisons à visiter.
Notre agent doit pouvoir consulter son itinéraire optimisé sur une carte.
Notre agent doit pouvoir saisir/modifier/supprimer des informations relatives à chaque visite comme le nom sur la boîte aux lettres, s’il a discuté avec le propriétaire ou s’il a distribué un prospectus, si l’habitant est propriétaire ou locataire, s’il y a un stop pub, la date visite et autre…

Directive technique :

Créer un backoffice en node.js qui à partir d’une liste de coordonnées GPS retournera un itinéraire de prospection optimisé. Nous discuterons ensemble de la solution envisagée pour le calcul de l’itinéraire avant de passer à l’implémentation.
Créer un client web en vue.js permettant de visualiser l’itinéraire sur une carte.
Pour la story 3, proposer un modèle de données qui permettra à l’agent de suivre sa prospection. Nous en discuterons avant de passer à l’étape 4.
CRUD de la prospection.

Délai du test technique: 

le test peut s’étaler sur 2 semaines maximum, les questions en cours de test sont les bienvenues, et même encouragées si utiles à l’avancement.

