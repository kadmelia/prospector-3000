# Application data

Quick summary of application data.

This is a proof of concept so there is obvious missing parts, like users management.

The Extra fields is a JSON field which purpose is to store various data about a property and a visit, while keeping data schema flexible. Also to demonstrate : hey, I know how to use JSON fields in postgres ;).

In a real world, I'll be adding a table to list available entries in "Extra" fields and their description, to make the UI on the tool evolutive by its users. For the PoC, and the sake of my sleep, this will be hardcoded on the UI.

More information on how theses data will be accessible by the API : [More informations here](../api/Readme.md)
 

## Property

Informations on a property and it occupants

|Nom|Type|Details|
|---|---|---|
|Identifier|ID||
|Address|Text||
|Zip code|Text||
|City|Text||
|Occupant's name|Text||
|Occupant's phone|Text||
|Occupant's email|Text||
|Occupant's quality|Enum : tenant or landlord||
|Extra|JSON|Dynamic collection of data about the property
|Visits|Collection of Visit|Link to property's visits|

 

## Visit

Informations about a property's visit by a salesperson

|Nom|Type|
|---|---|
|Identifier|ID|
|Date|DateTime|
|Extra|JSON|Dynamic collection of data about the visit|
|Comment|Text|
|Property|Property|



