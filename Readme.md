# Propspector 3000

Propspector 3000 is a real estate prospecting state of the art tool !

Well, no. Not really, but it's a demo project.

The goal is to have a solution to help real estate agent organize their prospection tours on the field.

This tool aims to manage properties, visits and travels between properties optimizations.

Project is split in 2 parts  : 

## Backend

The backend is using Node.js and Fastify to manage data access layer.
Data are stored in a Postgres database.

[More informations here](back/Readme.md)

## Frontend

Frontend is using Vue.js/Vuetify to build tool's UI.

[More informations here](front/Readme.md) 

## Provisionning

Application is ready to be deployed on a debian bare-metal server or VPS. 

[More informations here](provisionning/Readme.md) 