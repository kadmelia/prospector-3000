# Provisionning

If you need to deploy this site to a bare metal server, execute theses ansible scripts.

## Inventory

Configure the inventory 

```bash
cp inventory.yml.example inventory.yml
```

And adapt its values to your environment.

## Script

Execute scripts in this order : 

To install DB, webserver and Node.js

```bash
ansible-playbook -i inventory.yml site.yml
```

To create DB schema

```bash
ansible-playbook -i inventory.yml data.yml
```

To deploy website

```bash
ansible-playbook -i inventory.yml deploy.yml
```

To execut seeding script

```bash
ansible-playbook -i inventory.yml seed.yml
```